variable "gcp_project_id" {
  description = "Project's ID in the Google Cloud Platform"
  type        = string
  nullable    = false
}

variable "gcp_service_account_key_file_path" {
  description = "Path to the account key file linked to the project's ID"
  type        = string
  nullable    = false
}

variable "gce_instance_name" {
  description = "Name of the instance inside Google Cloud Platform"
  type        = string
  nullable    = false
}

variable "gce_instance_user" {
  description = ""
  type        = string
  nullable    = false
}

variable "gce_ssh_pub_key_file_path" {
  description = ""
  type        = string
  nullable    = false
}
